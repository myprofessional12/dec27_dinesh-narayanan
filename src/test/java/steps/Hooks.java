package steps;



import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase {
@Before
public void beforeScenarios(Scenario ba) {
	System.out.println(ba.getId());
	System.out.println(ba.getName());
	startReport();
	//report();
	test = extent.createTest(ba.getName(), ba.getId());
    test.assignAuthor("Dominic");
    test.assignCategory("smokes");
	startApp("chrome", "http://leaftaps.com/opentaps/");

}
@After
public void afterScenarios(Scenario ba) {
	System.out.println(ba.getStatus());
	close();
	stopReport();
}
}
