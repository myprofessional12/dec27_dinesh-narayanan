package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class CreateLead {
	ChromeDriver driver;
	/*@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webDriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
	    
	}



	@Given("load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps/");
		
	    
	}
*/
	@And("Enter the Username")
	public void enterTheUsername() {
		driver.findElementById("username").sendKeys("demosalesmanager");
	    
	}

	@Given("Enter the Password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	    
	}

	@When("click on the login button")
	public void clickOnTheLoginButton() {
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
	   
	}

	@Then("verify login is success")
	public void verifyLoginIsSuccess() {
	    System.out.println("Login is sucess");
	}

	@Then("click the CRF\\/SFA button")
	public void clickTheCRFSFAButton() {
	    driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("click the leads button")
	public void clickTheLeadsButton() {
		driver.findElementByLinkText("Leads").click();
	   
	}

	@Then("click the createLead button")
	public void clickTheCreateLeadButton() {
	    driver.findElementByXPath("//a[text()='Create Lead']").click();
	}

	@Then("Enter the companyName (.*)")
	public void enterTheCompanyName(String Company_Name) {
	    driver.findElementById("createLeadForm_companyName").sendKeys(Company_Name);
	}

	@Then("Enter the FirstName as (.*)")
	public void enterTheFirstName(String First_Name) {
		driver.findElementById("createLeadForm_firstName").sendKeys(First_Name);
	}

	@And("Enter the LastName (.*)")
	public void enterTheLastName(String Last_Name) {
		driver.findElementById("createLeadForm_lastName").sendKeys(Last_Name);
	}

	@Then("click the create lead submit button")
	public void clickTheCreateLeadSubmitButton() {
		driver.findElementByName("submitButton").click();
	}
	@Then("Create Lead is success")
	public void createLeadIsSuccess() {
	    System.out.println("Create Lead login success");	}

}
